#%%

import helpers as hp
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from itertools import groupby
from pylab import rcParams, figure, text, scatter, show
import numpy as np
import glob
import pickle
from sklearn.datasets import fetch_mldata
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
import pandas_profiling

# %matplotlib inline
import seaborn as sns
plt.style.use('seaborn-talk')

#%%

# helper function to load the data without resampling

# start and end dates are taken from the index loader config
# e.g, 

# v5
# start date: 2016-06-01
# end data: 2019-12-31


# v6 
# start date: 2016-01-01
# end data: 2019-12-31

def load_data(df, df_headers, start_date, end_date):
    """
    Parameters
    ----------
    df : pandas dataframe
        dataframe with sensor values as extraxted from getMachineData
    df_headers : pandas dataframe
        dataframe with tag names
    start_date : str
        start date from index loader config
    end_date : str
        end date from index loader config  
    """
    df_m = pd.concat([df_headers, df], axis=1)
    df_m = df_m.transpose()
    df_m.head()
    # Create a new variable called 'new_header' from the first row of 
    # the dataset
    # This calls the first row for the header
    new_header = df_m.iloc[0] 
    # take the rest of your data minus the header row
    df_m = df_m[1:] 
    # set the header row as the df header
    df_m.columns = new_header 
    date_index = pd.date_range(start=start_date, end=end_date, periods=len(df_m))
    df_m.set_index(date_index, inplace=True)
    return df_m

# with resampling

def load_resample_data(df, df_headers, start_date, end_date, sample_res):
    """
    Parameters
    ----------
    df : pandas dataframe
        dataframe with sensor values as extraxted from getMachineData
    df_headers : pandas dataframe
        dataframe with tag names
    start_date : str
        start date from index loader config
    end_date : str
        end date from index loader config
    sample_res : str
        string to determine resampling resultion      
    """
    df_m = pd.concat([df_headers, df], axis=1)
    df_m = df_m.transpose()
    df_m.head()
    # Create a new variable called 'new_header' from the first row of 
    # the dataset
    # This calls the first row for the header
    new_header = df_m.iloc[0] 
    # take the rest of your data minus the header row
    df_m = df_m[1:] 
    # set the header row as the df header
    df_m.columns = new_header 
    date_index = pd.date_range(start=start_date, end=end_date, periods=len(df_m))
    df_m.set_index(date_index, inplace=True)
    # upsample the machine to 60s:
    df_m = df_m.resample(sample_res).pad()
    return df_m
# %%

# load files from folder and get machine names
files = (glob.glob("C:\\Users\\Dima L\\Documents\\Presenso_local\\edp_project\\Group1_dima_mm_v6_data\\*.csv"))

# %%

# separate tag names and data files 
tagName_files = [] 
for idx, val in enumerate(files):
    if 'tagNames' in val:
        tagName_files.append(files.pop(idx))
   
machine_list = []
for i in range(len(files)):
    machine_list.append(files[i][81:-4])
# %%

data_dict = {}
for f, t, m in zip(files, tagName_files, machine_list):
    df_m = load_data(df=pd.read_csv(f), df_headers=pd.read_csv(t), start_date='2016-01-01', end_date='2019-12-31')
    data_dict[m] = df_m


# %%
data_dict['AGR500'].tail()

# %%

# pickle the file

pickle.dump(data_dict, open("Group1_mm_v6_data.p", "wb"))
# %%

# import from pickle

data_dict = pickle.load(open("Group1_mm_v6_data.p", "rb"))

# %%

# pandas profiling with 60 min resmapling:

for machine in machine_list:
    p_report = pandas_profiling.ProfileReport(data_dict[machine].resample('1d').pad())
    p_report.to_file(r'.\mm_v6\eda_' + machine+ '.html')


# %%

# dimensionality using PCA
# load y timestamps
df_y = pd.read_csv('GT_qs_us.csv')

# go over machines
for machine in machine_list:

    # scale features:
    sc = MinMaxScaler()
    sc_df = sc.fit_transform(data_dict[machine].values)

    # pca
    pca = PCA(n_components=3)
    pca_results = pca.fit_transform(sc_df)
    title_text = '{}: Explained variation per principal component: {}'.format(machine, np.around(pca.explained_variance_ratio_, decimals=2))
    # print('Explained variation per principal component: {}'.format(pca.explained_variance_ratio_))

    df_pca = pd.DataFrame()

    df_pca['pca-one'] = pca_results[:,0]
    df_pca['pca-two'] = pca_results[:,1] 
    df_pca['pca-three'] = pca_results[:,2]

    #set up y
    df_pca['y'] = 0
    for index, row in df_y.iterrows():
        df_pca['y'][(data_dict[machine].index >= row['event_start']) & (data_dict[machine].index <= row['event_end'])] = 1

    # plot in 2-D
    plt.figure(figsize=(16,10))
    sns.scatterplot(
        x="pca-one", y="pca-two",
        hue="y",
        palette=sns.color_palette("Set2", 2),
        data=df_pca,
        legend="full",
        # alpha=0.3
    )
    plt.title(title_text)
    plt.savefig(r'.\mm_v6\allPCA_' + machine+ '.png')
    # plt.show()

# %% PCA

# load y timestamps
df_y = pd.read_csv('GT_qs_us.csv')
# Only y==1

# go over machines
for machine in machine_list:

    # scale features:
    sc = MinMaxScaler()
    sc_df = sc.fit_transform(data_dict[machine].values)

    # pca
    pca = PCA(n_components=3)
    pca_results = pca.fit_transform(sc_df)
    title_text = '{}: Explained variation per principal component: {}'.format(machine, np.around(pca.explained_variance_ratio_, decimals=2))
    # print('Explained variation per principal component: {}'.format(pca.explained_variance_ratio_))

    df_pca = pd.DataFrame()

    df_pca['pca-one'] = pca_results[:,0]
    df_pca['pca-two'] = pca_results[:,1] 
    df_pca['pca-three'] = pca_results[:,2]

    #set up y
    df_pca['y'] = 0
    for index, row in df_y.iterrows():
        df_pca['y'][(data_dict[machine].index >= row['event_start']) & (data_dict[machine].index <= row['event_end'])] = 1
    # filter to y==0
    df_pca = df_pca[df_pca['y'] == 1]
    # plot in 2-D
    plt.figure(figsize=(16,10))
    sns.scatterplot(
        x="pca-one", y="pca-two",
        data=df_pca,
        legend="full",
        alpha=0.3
    )
    plt.title(title_text)
    plt.savefig(r'.\mm_v6\GTPCA_' + machine+ '.png')
    # plt.show()

# %%

# plot in 3-D

ax = plt.figure(figsize=(16,10)).gca(projection='3d')
ax.scatter(
    xs=df_pca["pca-one"], 
    ys=df_pca["pca-two"], 
    zs=df_pca["pca-three"], 
    c=df_pca["y"], 
)
ax.set_xlabel('pca-one')
ax.set_ylabel('pca-two')
ax.set_zlabel('pca-three')
plt.show()

# %%
