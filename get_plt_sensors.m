clusterActivate('prod');
customerActivate('edp');
ilc = getIndexLoaderConfig('Group1_v1');

% [data_stbl, dates_stbl, tagNames] = getMachineData('AGR500', ilc, 'selectedAnomalies_AVERAGE_aggregation_AVERAGE', ilc.anomalyCountStep);
[data, dates, tagNames] = getMachineData('Chumaceira_Turbina', ilc, 'rawForAllValues_AVERAGE', ilc.anomalyCountStep);

%% loop through machine
clusterActivate('dev');
customerActivate('edp2');
ilc = getIndexLoaderConfig('Group1_dima_mm_v4');

machineData = getMachines('Group1_dima_mm_v4', 'edp2');
names = {machineData.machine};

for i=1:length(names)
    name = names{i};
    [data, dates, tagNames] = getMachineData(name, ilc, 'rawForAllValues_AVERAGE', ilc.anomalyCountStep);
    dlmwrite(['C:\Users\Dima L\Documents\Presenso_local\edp_project\Group1_dima_mm_v4_data\data_' name '.csv'],data, 'delimiter', ',', 'precision', 15);
    T = cell2table(tagNames);
    % Write the table to a CSV file

    writetable(T,['C:\Users\Dima L\Documents\Presenso_local\edp_project\Group1_dima_mm_v6_data\data_' name '_tagNames.csv'],'WriteVariableNames',0)
end
%% use get original script

customerActivate('edp');
clusterActivate('prod');
[data, dates, tagNames, timeStamps] = getMachineDataOriginal('AGR500', 'Group1@', 'raw');


%% save to csv

% csvwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data.csv',data)


% save raw data manually:
GeneratorInd = 22;
CompensatorInd = 39;
PumpInd = 27;

% USELESS
% csvwrite('C:\Users\Dima L\Documents\Presenso_local\edp\data_generator.csv',data(GeneratorInd));
% csvwrite('C:\Users\Dima L\Documents\Presenso_local\edp\data_compensator.csv',data(CompensatorInd));
% csvwrite('C:\Users\Dima L\Documents\Presenso_local\edp\data_pump.csv',data(PumpInd));
% 
% csvwrite('C:\Users\Dima L\Documents\Presenso_local\edp\data_generator_ts.csv',timeStamps(GeneratorInd));
% csvwrite('C:\Users\Dima L\Documents\Presenso_local\edp\data_compensator_ts.csv',timeStamps(CompensatorInd));
% csvwrite('C:\Users\Dima L\Documents\Presenso_local\edp\data_pump_ts.csv',timeStamps(PumpInd));


%% USE THIS 
GeneratorInd = 22;
CompensatorInd = 39;
PumpInd = 27;
Idle = 19;
gridDisconnect = 23;
quickStop = 30;
urgencyStop = 31;
emergencyStop = 32;

normalStopGenerator = 36;
normalStopCompensator = 37;
normalStopPump = 38;

dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_normalStopGenerator.csv',data(normalStopGenerator), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_normalStopGenerator_ts.csv',timeStamps(normalStopGenerator), 'delimiter', ',', 'precision', 15);

dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_normalStopCompensator.csv',data(normalStopCompensator), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_normalStopCompensator_ts.csv',timeStamps(normalStopCompensator), 'delimiter', ',', 'precision', 15);

dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_normalStopPump.csv',data(normalStopPump), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_normalStopPump_ts.csv',timeStamps(normalStopPump), 'delimiter', ',', 'precision', 15);

dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_generator.csv',data(GeneratorInd), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_compensator.csv',data(CompensatorInd), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_pump.csv',data(PumpInd), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_Idle.csv',data(Idle), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_gridDisconnect.csv',data(gridDisconnect), 'delimiter', ',', 'precision', 15);


dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_generator_ts.csv',timeStamps(GeneratorInd), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_compensator_ts.csv',timeStamps(CompensatorInd), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_pump_ts.csv',timeStamps(PumpInd), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_Idle_ts.csv',timeStamps(Idle), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_gridDisconnect_ts.csv',timeStamps(gridDisconnect), 'delimiter', ',', 'precision', 15);


dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_quickStop.csv',data(quickStop), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_quickStop_ts.csv',timeStamps(quickStop), 'delimiter', ',', 'precision', 15);

dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_urgencyStop.csv',data(urgencyStop), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_urgencyStop_ts.csv',timeStamps(urgencyStop), 'delimiter', ',', 'precision', 15);

dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_emergencyStop.csv',data(emergencyStop), 'delimiter', ',', 'precision', 15);
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_emergencyStop_ts.csv',timeStamps(emergencyStop), 'delimiter', ',', 'precision', 15);

%% save raw4all
dlmwrite('C:\Users\Dima L\Documents\Presenso_local\edp_project\data_ct.csv',data, 'delimiter', ',', 'precision', 15);

%% for mean of all tags %%
close all;
GeneratorInd = 22;
CompensatorInd = 39;
PumpInd = 27;
Idle = 19;
gridDisconnect = 23;

% break timepoints to multiple segments
segments = 50;
interval = length(dates)/segments;

for s=0:segments-1
    disp(s)
    start_d = floor(s*interval +1);
    stop_d = floor(s*interval + interval);
    disp(start_d)
    disp(stop_d)
    clf();
    figure();
    ax1 = subplot(5,1,1);
    plot(dates(start_d:stop_d), data(GeneratorInd,start_d:stop_d), 'r');
    legend('Generator');
    ax2 = subplot(5,1,2);
    plot(dates(start_d:stop_d), data(CompensatorInd,start_d:stop_d), 'g');
    legend('Compensator');
    ax3 = subplot(5,1,3);
    plot(dates(start_d:stop_d), data(PumpInd,start_d:stop_d), 'm');
    legend('Pump');
    ax4 = subplot(5,1,4);
    plot(dates_stbl(start_d:stop_d), data_stbl(start_d:stop_d));
    legend('stability indicator');
    ax5 = subplot(5,1,5);
    plot(dates(start_d:stop_d), nanmean(data(:,start_d:stop_d),1));
    legend('mean of all sensors');
    
    linkaxes([ax1 ax2 ax3 ax4 ax5], 'x');
    fig = gcf;
    set(gcf, 'Position',  [100, 100, 5000, 4000]);
    saveas(gcf, ['C:\Presenso_local\edp\' int2str(s) '.png']);
end

close all;


%% for single tags %%

close all;
GeneratorInd = 22;
CompensatorInd = 39;
PumpInd = 27;

% break timepoints to multiple segments
segments = 50;
interval = length(dates)/segments;

for t=33:length(tagNames)
    mkdir(['C:\Presenso_local\edp\' tagNames{t}])
    if t== GeneratorInd || t== CompensatorInd || t== PumpInd
        continue
    end
    for s=0:segments-1
        disp(s)
        start_d = floor(s*interval +1);
        stop_d = floor(s*interval + interval);
        disp(start_d)
        disp(stop_d)
        clf();
        figure();
        ax1 = subplot(4,1,1);
        plot(dates(start_d:stop_d), data(GeneratorInd,start_d:stop_d), 'r');
        legend('Generator');
        ax2 = subplot(4,1,2);
        plot(dates(start_d:stop_d), data(CompensatorInd,start_d:stop_d), 'g');
        legend('Compensator');
        ax3 = subplot(4,1,3);
        plot(dates(start_d:stop_d), data(PumpInd,start_d:stop_d), 'm');
        legend('Pump');
        ax4 = subplot(4,1,4);
        plot(dates(start_d:stop_d), data(t,start_d:stop_d));
        legend(tagNames{t});
        
        linkaxes([ax1 ax2 ax3 ax4], 'x');
        fig = gcf;
        set(gcf, 'Position',  [100, 100, 5000, 4000]);
        saveas(gcf, ['C:\Presenso_local\edp\' tagNames{t} '\' int2str(s) '.png']);
        close all;
    end
end

%% for indicative tags %%

close all;
GeneratorInd = 22;
CompensatorInd = 39;
PumpInd = 27;

start_d = floor(s*interval +1);
stop_d = floor(s*interval + interval);


% break timepoints to multiple segments
segments = 50;
interval = length(dates)/segments;

        clf();
        figure();
        ax1 = subplot(3,1,1);
        plot(dates(start_d:stop_d), data(GeneratorInd,start_d:stop_d), 'r');
        legend('Generator');
        ax2 = subplot(3,1,2);
        plot(dates(start_d:stop_d), data(CompensatorInd,start_d:stop_d), 'g');
        legend('Compensator');
        ax3 = subplot(3,1,3);
        plot(dates(start_d:stop_d), data(PumpInd,start_d:stop_d), 'm');
        legend('Pump');
        
        linkaxes([ax1 ax2 ax3], 'x');
        fig = gcf;
        set(gcf, 'Position',  [100, 100, 5000, 4000]);
%         saveas(gcf, ['C:\Presenso_local\edp\' tagNames{t} '\' int2str(s) '.png']);
%         close all;
%     end
% end
