
clusterActivate('dev');
customerActivate('edp2');
ilc = getIndexLoaderConfig('Group1_dima_mm_v6');
data_path = 'C:\Users\Dima L\Documents\Presenso_local\edp_project\Group1_dima_mm_v6_data\';

machineData = getMachines('Group1_dima_mm_v6', 'edp2');
names = {machineData.machine};

% loop through machine
for i=1:length(names)
    machineName = names{i};
    % get the raw data
    [data, dates, tagNames, timeStamps] = getMachineDataOriginal(machineName, 'Group1@', 'raw');
    % save tag names
    T = cell2table(tagNames);
    writetable(T,[data_path machineName '_tagNames.csv'],'WriteVariableNames',0)
    %create a folder for tags
    mkdir([data_path machineName])
    % loop over tags 
    for j=1:length(tagNames)
        tagName = tagNames{j};
        dlmwrite([data_path machineName '\' tagName '.csv'], data(j), 'delimiter', ',', 'precision', 15);
        dlmwrite([data_path machineName '\' tagName '_ts.csv'], timeStamps(j), 'delimiter', ',', 'precision', 15);
    end
end