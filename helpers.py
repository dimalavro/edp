# Dependencies 
from plotly import tools
import plotly.graph_objects as go
import plotly.express as px 
import plotly.figure_factory as ff
from plotly.subplots import make_subplots
import pandas_profiling
import pandas as pd
from pylab import rcParams, figure, text, scatter, show
import numpy as np
import matplotlib.pyplot as plt


def plot_multi_bar(*args):
    fig = go.Figure()
    for name, df in args:
        fig.add_trace(
        go.Bar(name = name,
              x=df['index'],
              y=df['value']
              ))
    fig.show()
    

def plot_multi_hist(df, var_list):
    fig = make_subplots(len(var_list), cols=1)
    for index, item in enumerate(var_list):
        hist = go.Histogram(x=df[item], name=item)
        fig.append_trace(hist, index+1, 1)
    fig.show()
    

def create_profile(df, df_headers):
    df_m = pd.concat([df_headers, df], axis=1)
    df_m = df_m.transpose()
    df_m.head()
    # Create a new variable called 'new_header' from the first row of 
    # the dataset
    # This calls the first row for the header
    new_header = df_m.iloc[0] 
    # take the rest of your data minus the header row
    df_m = df_m[1:] 
    # set the header row as the df header
    df_m.columns = new_header 
    # Lets see the 5 first rows of the new dataset
    df_m.head()
    date_index = pd.date_range(start='2016-01-01', end='2019-12-01', periods=len(df_m)) # REMEMBER to check the periods correspondance with the actual lenght of df_m
    df_m.set_index(date_index, inplace=True)
    # upsample the machine to 60s:
    df_m = df_m.resample('60s').pad()
    df_m.shape
    return pandas_profiling.ProfileReport(df_m.astype('float64').resample('1d').pad())


def load_resample_data(df, df_headers, start_date, end_date, sample_res):
    df_m = pd.concat([df_headers, df], axis=1)
    df_m = df_m.transpose()
    df_m.head()
    # Create a new variable called 'new_header' from the first row of 
    # the dataset
    # This calls the first row for the header
    new_header = df_m.iloc[0] 
    # take the rest of your data minus the header row
    df_m = df_m[1:] 
    # set the header row as the df header
    df_m.columns = new_header 
    date_index = pd.date_range(start=start_date, end=end_date, periods=len(df_m))
    df_m.set_index(date_index, inplace=True)
    # upsample the machine to 60s:
    df_m = df_m.resample(sample_res).pad()
    return df_m


def len_raw_tags(df, name):
    '''
    plots the distribution of durations of events in a given raw tag
    '''   
    #build list section
    streak_list = []
    begin_date = 0
    for i in range(len(df)):
        if (df.iloc[i].value == 1) & (begin_date == 0):
            begin_date = df.index[i]
        elif (df.iloc[i].value == 0) & (begin_date != 0):
            streak_list.append((df.index[i] - begin_date).total_seconds()/3600) # in hours
            begin_date = 0
    
    #plot section
    mean = np.mean(streak_list)
    median = np.median(streak_list)
    minimum = np.min(streak_list)
    maximum = np.max(streak_list)
    plot_string = "Mean: {0:.2f} h\n Median {1:.2f} h\n Min: {2:.2f} h; Max: {3:.2f} h" #4 {} placeholders
    plt.figure()
    plt.hist(streak_list, bins=100, color='lightcoral')
    plt.text(0.5, 0.5,plot_string.format(mean, median, minimum, maximum),
        horizontalalignment='center',
        verticalalignment='top', transform=plt.gca().transAxes, color='darkblue', fontweight= 'heavy', size='xx-large')
    plt.title(name)
    plt.xlabel('mode duration in hours')
    plt.ylabel('frequency')
    plt.savefig(r'.\mm_v3\_' + name+ '.png')
    

def get_event_timestamps(df):
    beginDate_list = []
    endDate_list = []
    begin_date = 0
    for i in range(len(df)):
        if (df.iloc[i].value == 1) & (begin_date == 0):
            begin_date = df.index[i]
        elif (df.iloc[i].value == 0) & (begin_date != 0):
            beginDate_list.append(begin_date.strftime("%d/%m/%Y %H:%M"))
            endDate_list.append(df.index[i].strftime("%d/%m/%Y %H:%M"))
            begin_date = 0
    return beginDate_list, endDate_list
    
